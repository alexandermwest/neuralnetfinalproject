#main implementation from chesstwitch found here:
# https://github.com/geohot/twitchchess/

from __future__ import print_function
import os
import chess
import time
import torch
import chess.svg
import traceback
import numpy as nmp
import base64
from state import State
from training import Net

class Evaluator(object):
	def __init__(self):
		vals = torch.load("nets/value.pth", map_location=lambda storage, loc: storage)
		self.model = Net()
		self.model.load_state_dict(vals)

	# will return likeliness to win from 0.0 -> 1.0 from any move on the board
	def __call__(self, s):
		brd = s.serialize()[None]
		output = self.model(torch.tensor(brd).float())
		return float(output.data[0][0])
	
	
MAXVAL = 10000
class ClassicValuator(object):
  values = {chess.PAWN: 1,
            chess.KNIGHT: 3,
            chess.BISHOP: 3,
            chess.ROOK: 5,
            chess.QUEEN: 9,
            chess.KING: 0}

  def __init__(self):
    self.reset()
    self.memo = {}

  def reset(self):
    self.count = 0

  # writing a simple value function based on pieces
  # good ideas:
  # https://en.wikipedia.org/wiki/Evaluation_function#In_chess
  def __call__(self, s):
    self.count += 1
    key = s.key()
    if key not in self.memo:
      self.memo[key] = self.value(s)
    return self.memo[key]

  def value(self, s):
    b = s.board
    # game over values
    if b.is_game_over():
      if b.result() == "1-0":
        return MAXVAL
      elif b.result() == "0-1":
        return -MAXVAL
      else:
        return 0

    val = 0.0
    # piece values
    pm = s.board.piece_map()
    for x in pm:
      tval = self.values[pm[x].piece_type]
      if pm[x].color == chess.WHITE:
        val += tval
      else:
        val -= tval

    # add a number of legal moves term
    bak = b.turn
    b.turn = chess.WHITE
    val += 0.1 * b.legal_moves.count()
    b.turn = chess.BLACK
    val -= 0.1 * b.legal_moves.count()
    b.turn = bak

    return val

def computer_minimax(s, v, depth, a, b, big=False):
  if depth >= 2 or s.board.is_game_over():
    return v(s)
  # white is maximizing player
  turn = s.board.turn
  if turn == chess.WHITE:
    ret = -MAXVAL
  else:
    ret = MAXVAL
  if big:
    bret = []

  # can prune here with beam search
  isort = []
  for e in s.board.legal_moves:
    s.board.push(e)
    isort.append((v(s), e))
    s.board.pop()
  move = sorted(isort, key=lambda x: x[0], reverse=s.board.turn)

  # beam search beyond depth 3
  if depth >= 2:
    move = move[:10]

  for e in [x[1] for x in move]:
    s.board.push(e)
    tval = computer_minimax(s, v, depth+1, a, b)
    s.board.pop()
    if big:
      bret.append((tval, e))
    if turn == chess.WHITE:
      ret = max(ret, tval)
      a = max(a, ret)
      if a >= b:
        break  # b cut-off
    else:
      ret = min(ret, tval)
      b = min(b, ret)
      if a >= b:
        break  # a cut-off
  if big:
    return ret, bret
  else:
    return ret

def explore_leaves(s, v):
  ret = []
  start = time.time()
  v.reset()
  bval = v(s)
  cval, ret = computer_minimax(s, v, 0, a=-MAXVAL, b=MAXVAL, big=True)
  eta = time.time() - start
  print("%.2f -> %.2f: explored %d nodes in %.3f seconds %d/sec" % (bval, cval, v.count, eta, int(v.count/eta)))
  return ret


# chess board and engine
e = ClassicValuator()
s = State()

computerWhite=0
turnNumber=1
theColor="white's"
	
# encodes the board into the webpage to prevent blinking on refresh
def to_svg(s):
	return base64.b64encode(chess.svg.board(board=s.board).encode('utf-8')).decode('utf-8')
	
		


from flask import Flask, Response, request
app = Flask(__name__)

@app.route("/")
def hello():
	# encodes the board into the webpage to prevent blinking on refresh
	ret = open("index.html").read()
	return ret.replace('start', s.board.fen())
"""
board_svg = to_svg(s)
ret = '<html><head>'
ret += '<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>'
ret += '<style>input { font-size: 30px; } button { font-size: 30px; }</style>'
ret += '<a href="/selfplay"> Play vs itself</a><br/>'
ret += '</head><body>'
ret += '<img width=600 height=600 src="data:image/svg+xml ;base64,%s"></img><br/>' % board_svg
ret += '<form action ="/move"><input id = "move" name = "move" type="text"></input><input type = "submit" value="Move"></form><br/>'
#ret += '<button onclick=\'$.post("/move"); location.reload();\'>MAKE COMPUTER MOVE</button>'
ret += '<script>$(function(){var input = document.getElementById("move"); console.log("selected"); input.focus(); input.select()});</script>'
return ret
"""
"""
@app.route("/board.svg")
def board():
	return Response(chess.svg.board(board=s.board), mimetype='image/svg+xml')
"""
	
def computer_move(s, e):
	# computer move
	global theColor
	global turnNumber
	if computerWhite == 0:
		theColor="white's"
		print(theColor, computerWhite)
	else:
		theColor="black's"
		print(theColor, computerWhite)
	move = sorted(explore_leaves(s, e), key=lambda x: x[0], reverse=s.board.turn)
	print("top 3:")
	for i,m in enumerate(move[0:3]):
		print(" ", m)
	s.board.push(move[0][1])
	turnNumber += 1
	if computerWhite == 0:
		theColor="black's"
	else:
		theColor="white's"
	
def getColor():
	return theColor
	
@app.route("/selfplay")
def selfplay():
	s = State()
	ret = '<html><head>'
	
	# plays against itself
	while not s.board.is_game_over():
		computer_move(s, e)
		ret += '<img width=600 height=600 src="data:image/svg+xml ;base64,%s"></img><br/>' % to_svg(s)
		print(s.board)
		#if s.board.turn == chess.WHITE:
	print(s.board.result())
	
	return ret

# chess algebraic notation for chess movement
# decided against it and to use moveable objects in js
@app.route("/move")
def move():
	global theColor
	if not s.board.is_game_over():
		move = request.args.get('move', default="")
		if move is not None and move != "":
			print("human moves", move)
			try:
				s.board.push_san(move)
				computer_move(s, e)
			except Exception:
				traceback.print_exc()
			reponse = app.response_class(
				response=s.board.fen(),
				status=200
			)
			
			return response
	else:
		print("GAME IS OVER")
		response=app.response_class(
			response="game oer",
			status=200
		)
		return response
	return hello()
		
"""	
@app.route("/move", methods=["POST"])
def move():
	computer_move()
	return ""
"""

# moves given as coordinates of piece moved
@app.route("/move_coordinates")
def move_coordinates():
  if not s.board.is_game_over():
    source = int(request.args.get('from', default=''))
    target = int(request.args.get('to', default=''))
    promotion = True if request.args.get('promotion', default='') == 'true' else False

    move = s.board.san(chess.Move(source, target, promotion=chess.QUEEN if promotion else None))
    if move is not None and move != "":
      print("human moves", move)
      global turnNumber
      try:
        s.board.push_san(move)
        turnNumber += 1
        computer_move(s, e)
      except Exception:
        traceback.print_exc()
    response = app.response_class(
      response=s.board.fen(),
      status=200
    )
    return response

  print("GAME IS OVER")
  response = app.response_class(
    response="game over",
    status=200
  )
  return response
	
@app.route("/newgame")
def newgame():
  s.board.reset()
  response = app.response_class(
    response=s.board.fen(),
    status=200
  )
  return response

@app.route("/newBlackGame")
def newBlackGame():
  s.board.reset()
  global computerWhite
  global theColor
  global turnNumber
  computerWhite=0
  turnNumber=1
  theColor="white's"
  computer_move(s, e)
  response = app.response_class(
    response=s.board.fen(),
    status=200
  )
  computerWhite=0
  return response
  
@app.route("/getTurnNumber")
def getNumber():
	return str(turnNumber) 

@app.route("/currentColor")
def getStatus():
    return theColor
  
@app.route("/newWhiteGame")
def newWhiteGame():
  s.board.reset()
  global computerWhite
  global theColor
  global turnNumber
  computerWhite=1
  turnNumber=1
  theColor="white's"
  response = app.response_class(
    response=s.board.fen(),
    status=200
  )
  computerWhite=1
  return response
  
@app.route("/get_player_color")
def returnPlayerColor():
	return str(computerWhite)


if __name__ == "__main__":
	s = State()
	if os.getenv("SELFPLAY") is not None:
		while not s.board.is_game_over():
			computer_move(s, e)
			print(s.board)
			print(s.board.result())
	else:
  		#computerWhite = nmp.random.randint(0,2)
  		if computerWhite == 0:
  			computer_move(s,e)
  		app.run(debug=True)