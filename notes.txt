• Establish the search tree
• Use a neural net to prune the search tree

Defintion: Value network
V = f(state)

What is V?
V = -1 black wins board state
V = 0 draw board state
V = 1 white wins board state


State(Board):

Pieces(2+7*2 = 16)
• Universal
** Blank
** Blank (En passant)
* Pieces
** Pawn
** Bishop
** Knight
** Rook
** Rook (can castle)
** Queen
** King

Extra state:
* To move

8x8x5 = 257 bit (vector of 0 or 1)

REQUIREMENTS:
install chess-python
install numpy
install cuda
install torchpy (based on python/cuda version and use pip with download selection)
install flask

run CUDA_VISIBLE_DEVICES=0 python3 training.py
it will enable cuda. is not possible on mac