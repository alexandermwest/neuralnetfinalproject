#main implementation from chesstwitch found here:
# https://github.com/geohot/twitchchess/

import chess
import numpy as nmp
from training import Net
import torch

# base class
class State(object):	
	# init the board state
	def __init__(self, board=None):
		if board is None:
			self.board = chess.Board()
		else:
			self.board = board;
			
	def key(self):
		return (self.board.board_fen(), self.board.turn, self.board.castling_rights, self.board.ep_square)
		
	# serialize the inputted data to be interpreted
	def serialize(self):
	
		# chess-py checks if the current board is in a valid state
		# will disregard invalid board states in the pgns if there are any
		assert self.board.is_valid()
		
		# creates an empty state of size 64
		# is the binary state of the board
		state = nmp.zeros(64, nmp.uint8)
		
		# for each index
		for pieces in range(64):
		
			# grab the piece in that index
			indexPiece = self.board.piece_at(pieces)
			
			# if that piece exists
			if indexPiece is not None:
				# assign that piece based on value
				state[pieces] = {"P": 1, "N": 2, "B": 3, "R": 4, "Q": 5, "K": 6, \
                    				 "p": 9, "n":10, "b":11, "r":12, "q":13, "k": 14}[indexPiece.symbol()]
				
		# check for castling on either side of board for either opponent
		if self.board.has_queenside_castling_rights(chess.WHITE):
			assert state[0] == 4
			state[0] = 7
		if self.board.has_kingside_castling_rights(chess.WHITE):
			assert state[7] == 4
			state[7] = 7
		if self.board.has_queenside_castling_rights(chess.BLACK):
			assert state[56] == 8+4
			state[56] = 8+7
		if self.board.has_kingside_castling_rights(chess.BLACK):
			assert state[63] == 8+4
			state[63] = 8+7
				
		# chess-py to check if it can perform en passant
		if self.board.ep_square is not None:
			assert state[self.board.ep_square] == 0
			state[self.board.ep_square] = 8
		state = state.reshape(8, 8)
		
		# generate an array of 0's of size 8x8x5 (our board state size)
		boardState = nmp.zeros((5,8,8), nmp.uint8)
		
		# zero -> third column to binary
		boardState[0] = (state>>3)&1
		boardState[1] = (state>>2)&1
		boardState[2] = (state>>1)&1
		boardState[3] = (state>>0)&1
		
		# fourth column is loaded in as who's turn it is
		boardState[4] = (self.board.turn*1.0)
		
		# 257 bits of data (pieces, states and featured moves)
		
		# uses chess-py function to extract the relevant board data
		# serializedBoardState = self.board.shredder_fen()
		return boardState

	# return the list of legal moves from the current board state		
	def edges(self):
		# add neural net here
	 	return list(self.board.legal_moves)

# return the piece value	
def value(self):
	return list(self.board.legal_moves)

if __name__ == "__main__":
	model = Net()
	s = State()
	
	torch.load("nets/value.pth")
	#print(s.edges())
	#print(s.serialize())