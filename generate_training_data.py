#main implementation from chesstwitch found here:
# https://github.com/geohot/twitchchess/

import os
import chess.pgn
import numpy as nmp
from state import State

import ctypes

class disable_file_system_redirection:
	_disable = ctypes.windll.kernel32.Wow64DisableWow64FsRedirection
	_revert = ctypes.windll.kernel32.Wow64RevertWow64FsRedirection
	def __enter__(self):
		self.old_value = ctypes.c_long()
		self.success = self._disable(ctypes.byref(self.old_value))
	def __exit__(self, type, value, traceback):
		if self.success:
			self._revert(self.old_value)

def get_dataset(sample_number=None):
	gameNumber = 0;
	X,Y = [], []
	values = {'1/2-1/2': 0, '0-1': -1, '1-0': 1}

	# loop through the data sets and read in pgns
	# dir name needs to be in the same directory as .py files
	# 'data' is the name of the dir
	with disable_file_system_redirection():
		for fn in os.listdir("data"):
			pgn = open(os.path.join("data", fn))
		
			# loop over every game in the pgn
			while 1:
				# try to read another game
				# if there is a game read it
				game = chess.pgn.read_game(pgn)
				
				if game is None:
					break
				
				# find out who won the game based on pgn
				# find this with the keyword Result in the pgn game block
				# results can be 1 - 0 || 0 - 1 || 1/2 - 1/2 || WHITE || BLACK || TIE
				results = game.headers['Result']
				if results not in values:
					continue
				value = values[results]
				
					
			
				# prints the end win state
				#print(value)
			
				# assign current board
				board = game.board()
			
				# loop through moves in pgn and add them to a game state
				for i, move in enumerate(game.main_line()):
					board.push(move)
				
					"""
					# prints the current number of moves
					print(i)
				
					# prints out the winner
					print(result)
				
					# print out each addition
					print(board)
					"""
					# extract the boards by using state's function serialize 
					serializeData = State(board).serialize()
					X.append(serializeData)
					Y.append(value)
				
				print("parsing game %d, got %d examples" % (gameNumber, len(X)))
				# X is number of games, cap this number at the number of training info you want
				if sample_number is not None and len(X) > sample_number:
					return X,Y
				gameNumber += 1;
			X = nmp.array(X)
			Y = nmp.array(Y)
		return X,Y
				
			
				# print the boards in binary serialized states
				#print(value, serializeData);
	
if __name__== "__main__":
	# pass in the number of sample moves you would like to be used for training			
	X,Y = get_dataset(5000000)
	
	# write relevant data collected out to a compressed source file
	nmp.savez("processed/dataset_100k.npz", X, Y)