#main implementation from chesstwitch found here:
# https://github.com/geohot/twitchchess/

import numpy as nmp
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset
from torch import optim

class ChessValueDataSet(Dataset):
	def __init__(self):
		# pull data from specified location for training
		data = nmp.load("processed/dataset_100k.npz")
		self.X = data['arr_0']
		self.Y = data['arr_1']
		print("loaded", self.X.shape, self.Y.shape)
		
	def __len__(self):
		return self.X.shape[0]
		
	def __getitem__(self, idx):
		return (self.X[idx], self.Y[idx])

class Net(nn.Module):
	def __init__(self):
		super(Net, self).__init__()
		
		# represents the various pixel sizes for convolution net
		# merely a filtered version of original board
		self.a1 = nn.Conv2d(5, 16, kernel_size=3, padding=1)
		self.a2 = nn.Conv2d(16, 16, kernel_size=3, padding=1)
		self.a3 = nn.Conv2d(16, 32, kernel_size=3, stride=2)
		
		self.b1 = nn.Conv2d(32, 32, kernel_size=3, padding=1)
		self.b2 = nn.Conv2d(32, 32, kernel_size=3, padding=1)
		self.b3 = nn.Conv2d(32, 64, kernel_size=3, stride=2)
	
		self.c1 = nn.Conv2d(64, 64, kernel_size=2, padding=1)
		self.c2 = nn.Conv2d(64, 64, kernel_size=2, padding=1)
		self.c3 = nn.Conv2d(64, 128, kernel_size=2, stride=2)
	
		self.d1 = nn.Conv2d(128, 128, kernel_size=1)
		self.d2 = nn.Conv2d(128, 128, kernel_size=1)
		self.d3 = nn.Conv2d(128, 128, kernel_size=1)
	
		self.last = nn.Linear(128, 	1)
	
	
	
	def forward(self, x):
	
		# relu converts all negative pixel sizes to 0
		# will remove a lot of 'noise' from the smaller images
		# by pooling it will further remove noise by taking the largest size within a 
		# particular area 
		
		# can read more here: https://blog.algorithmia.com/convolutional-neural-nets-in-pytorch/
		x = F.leaky_relu(self.a1(x)).cuda()
		x = F.leaky_relu(self.a2(x)).cuda()
		x = F.leaky_relu(self.a3(x)).cuda()
		
		
		# 4x4
		x = F.leaky_relu(self.b1(x)).cuda()
		x = F.leaky_relu(self.b2(x)).cuda()
		x = F.leaky_relu(self.b3(x)).cuda()
		
		
		# 2x2	
		x = F.leaky_relu(self.c1(x)).cuda()
		x = F.leaky_relu(self.c2(x)).cuda()
		x = F.leaky_relu(self.c3(x)).cuda()
		
		
		# 1x128
		x = F.leaky_relu(self.d1(x)).cuda()
		x = F.leaky_relu(self.d2(x)).cuda()
		x = F.leaky_relu(self.d3(x)).cuda()
		
		

		x = x.view(-1, 128)
		x = self.last(x).cuda()
		
		# value output
		# was F.tanh(x)
		# apparently it was depreciated 
		# might need to retrain?
		return torch.tanh(x).cuda()
if __name__ == "__main__":	
	device = "cpu"

	chess_dataset = ChessValueDataSet()
	train_loader = torch.utils.data.DataLoader(chess_dataset, batch_size=256, shuffle=True)
	model = Net()
	model = model.cuda()
	optimizer = optim.Adam(model.parameters(), lr=0.00001, betas=(0.9, 0.999),eps=1e-08)
	floss = nn.MSELoss()

	# to use cuda replace cpu and uncomment line under
	#"cuda"

	#if device == "cuda":
	#	model = model.cuda()

	model.train()

	for epoch in range(100):
		all_loss = 0
		num_loss = 0
		for batch_idx, (data, target) in enumerate(train_loader):
			target = target.unsqueeze(-1)
			data, target = data.to(device), target.to(device)
			data = data.float()
			target = target.float()
	
			# print for training data
			#print(data.shape, target.shape)
			optimizer.zero_grad()
			output = model(data)
	
			# print for training data
			#print(output.shape)
	
			loss = floss(output, target)
			loss.backward()
			optimizer.step()
		
			all_loss += loss.item()
			num_loss += 1
		
		print("%3d: %f" % (epoch, all_loss/num_loss))
		torch.save(model.state_dict(), "nets/value.pth")
		#print("%3d: %f" % (epoch, all_loss))